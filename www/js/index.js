
var app = 
{
    // Application Constructor
    initialize: function() 
    {
        document.addEventListener('deviceready', this.onDeviceReady(), false);
    },

    onDeviceReady: function() 
    {
        this.goHome();
    },

    /**
     * goHome
     * Go to the main menu start page
     */
    goHome: function() 
    {
        if (!$("#homescreen").length) {
            this.createObject('div','homescreen','homeScreen','.app');
        }
        this.createObject('div','menu_odd',null,'#homescreen');
        this.createObject('div','menucell_odd',null,'#menu_odd');
        this.createObject('div','menu_pair',null,'#homescreen');
        this.createObject('div','menucell_pair',null,'#menu_pair');
        this.menu();
    },

    menu: function() 
    {
        var size = Object.keys(menu).length;
        for (var i = 0; i < size; i++) 
        {
            if (i % 2) 
            { // odd
                $("#menucell_odd").append('<button id="' + menu[i].action +
                '" class="itenmenu odditem">'+
                menu[i].label +
                '</button>');
                document.getElementById(menu[i].action)
                    .addEventListener("click", this.action);
            }
            else 
            { // pair
                $("#menucell_pair").append('<button id="' + menu[i].action +
                '" class="itenmenu odditem">'+
                menu[i].label +
                '</button>');
                document.getElementById(menu[i].action)
                    .addEventListener("click", this.action);
            }



        }
    },

    action: function(act) {
        switch (this.id) {
            case 'enterroom':
                var room = prompt(spoke('waroom'));
                if (room==null ||
                    isNaN(room)
                    ) 
                    {
                        break;
                    }
                app.enterroom(this.id,'room=' + room);
                break;
            case 'createroom':
                app.enterroom(this.id,'room=0');
                break;
            case 'help':
                app.openhelp();
                break;
            default:
                alert('Menu miss: 0 Contact the developers');
                break;
        }
    },

    /**
     * createObject
     * 
     * Create element Object on HTML app scene
     * The name maybe used to class too with you writed using '_'
     * 
     * @param {String} type 
     * @param {String} name 
     * @param {String} clsName 
     * @param {String} context 
     */
    createObject: function(type, name, clsName, context) 
    {
        if (!clsName) { clsName = name.split('_')[0]; }
        var obj = document.createElement(type);
        obj.id = name;
        obj.className = clsName;
        $(context).append(obj);
        return obj;
    },

    enterroom: function(serv,data) {
        app.waiting(true);
        $.ajax({
            url:ROOTURL+serv+'/' ,
            type:'get',
            dataType:'json',
            crossDomain: true,
            data: data + noCache('&'),
            complete: function(x,s) {
                app.waiting(false);
                if (s=='success') {
                    app.manifestRoom(x.responseJSON.room);
                } else {
                    alert(s + ' ' + x.status + ": " + x.statusText);
                }
            }
        });
    },

    manifestRoom: function(room) {
        $(".menu").remove();

        var turncards = app.createObject('button','turncards','turnCards','#homescreen');
        var roomnumber = app.createObject('div','roomnumber','roomNumber','#homescreen');
        var takeacard = app.createObject('button','takeacard','takeAcard','#homescreen');
       
        $(roomnumber).html(spoke('room') + " <span>"+room+"</span>");
        $(turncards).html(spoke('turn'));
        $(takeacard).html(spoke('take')).click(function(){
            app.takeacard();
        });

        app.putElMiddle(turncards);

        $(turncards).click(function() {
            app.waiting(true);
            $.ajax({
                url:ROOTURL+'turncards/',
                type:'get',
                dataType:'json',
                crossDomain: true,
                data: "room="+$("#roomnumber span").text() + noCache('&'),
                complete: function(x,s) {
                    app.waiting(false);
                    if (s=='success') {
                        
                        var size = Object.keys(x.responseJSON).length;
                        
                        if (size==0) {
                            alert("No one of members pick a card");
                            return;
                        }

                        $("#turncards").remove();
                        var divcards = app.createObject('div',
                                    'divimgcard','divimgcard','#homescreen');

                        var cards = new Array();
                        var lastminor = 14;
                        var lastmaior = 0;
                        var minor= 0, major=0;
                        for (var i = 0; i < size; i++) {
                            cards[i] = app.createObject('div',
                                    'imgcard'+i,'imgcard','#divimgcard');
                            $(cards[i]).html(
                                '<img src="img/cards/style_00/' + 
                                    ('0'+x.responseJSON[i].card).substr(-2) +'.jpg">'+
                                   '<label>' + x.responseJSON[i].name + '</label>');


                            if (x.responseJSON[i].card<lastminor) {
                                minor = i;
                                lastminor=x.responseJSON[i].card;
                            }
                            if (x.responseJSON[i].card>lastmaior) {
                                major = i;
                                lastmaior=x.responseJSON[i].card;
                            }

                        }

                        $(cards[major]).css({
                            "box-shadow":'0 0 1em gold, 0 0 1em red',
                            'background-color':'#222'});
                        $(cards[minor]).css({
                            "box-shadow":'0 0 1em white, 0 0 1em blue',
                            'background-color':'#222'});

                        setTimeout(function() {
                        app.putElCenter(divcards);
                        app.putElMiddle(divcards);

                        $(divcards).css({'visibility':'visible'});
                        }, 100);

                    } else {
                        alert(s + ' ' + x.status + ": " + x.statusText);
                    }
                }
            });
        });

    },

    waiting: function(k) {
        if (k) {
            $("#wait").css({'visibility':'visible'});
        } else {
            $("#wait").css({'visibility':'hidden'});
        }
    },

    putElMiddle: function(obj) {
        var half = parseInt($(obj).outerHeight()/2);
        $(obj).css({'top':'50%','margin-top':'-'+half+'px'});
    },

    putElCenter: function(obj) {
        var half = parseInt($(obj).outerWidth()/2);
        $(obj).css({'left':'50%','margin-left':'-'+half+'px'});
    },

    openhelp: function() {
        $(".help").css({
            'visibility':'visible'
        }).click(function(){
            $(this).css({'visibility':'hidden'});
        });
    },

    takeacard: function() {
        app.createObject("div",'boxcards','boxCards','#homescreen');
        
        var trailcards = app.createObject("div",'trailcards','trailCards','#boxcards');

        var cards = new Array();
        var fib = 0;
        for (var i = 1; i < 7; i++) {
            fib = fibonacci(i);
            cards[i] = app.createObject("card",'card'+i,'Cards','#trailcards');
            $(cards[i]).data('cardValue',fib);
            $(cards[i]).append("<img src='img/cards/style_00/"+
                ('0'+fib).substr(-2)+".jpg'>").click(function() {
                    app.showCard($(this).data('cardValue'));
                });
        }



    },

    showCard: function(value) {
        var showcard = app.createObject('div','thecard','theCard','#homescreen');
        $(showcard).html('<div><img src="img/cards/style_00/'+ ('0'+value).substr(-2) +'.jpg"></div>')
        .data('cardValue',value).click(function(){ 
            app.choose($(this).data('cardValue')); 
        });
        app.putElMiddle(showcard);
    },

    choose: function(value) {
        if (CHOOSEN) { return false; }
        var choo = app.createObject('div','chs','chs','#thecard');
        $(choo).html('<button onclick="app.chooseCard(' + value + ')">'+ spoke('that') +'</button><br>'+
                        '<button onclick="app.dontChoose()">'+ spoke('maybe') +'</button>');
        return true;
    },

    dontChoose: function() {
        $('#thecard').remove();
    },

    chooseCard(card) {
        var name = prompt(spoke('youtname'));
        if (name == null) {
            return false;
        }
        $.ajax({
            url:ROOTURL+'choosecad/',
            data:'name=' + name +'&card='+card+'&room='+$('#roomnumber span').text()+noCache('&'),
            type:'get',
            crossDomain: true,
            complete: function(x,s) {
                if (s=='success') {
                    if (x.responseJSON.status=='noProblem') {
                        $('.chs').remove();
                        $(".confirmCard").css({
                            'visibility':'visible'
                        }).html(spoke(''));
                        CHOOSEN = true;
                    }
                } else {
                    alert(x.status+': '+x.statusText);
                }
            }
        });
    }
};

function noCache(cnt) {
    if (cnt==undefined) {
        cnt = '?';
    }
    return cnt+'abc' + (Math.random() * 10) + '=' + (Math.random() * 100000000);
}

function fibonacci(num, memo) {
    memo = memo || {};
    if (memo[num]) return memo[num];
    if (num <= 1) return 1;
    return memo[num] = fibonacci(num - 1, memo) + fibonacci(num - 2, memo);
  }


var ROOTURL = "https://wiwon.undersprints.com/";
var CHOOSEN = false;

var menu = 
{
    0:{'label':spoke('enterroom'),'action':'enterroom'},
    1:{'label':spoke('createroom'),'action':'createroom'},
    2:{'label':spoke('help'),'action':'help'}
};


app.initialize();
