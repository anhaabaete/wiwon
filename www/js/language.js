sentence = {
    'en':{
        'createroom':'Create A Room',
        'enterroom':'Enter A Room',
        'help':'Help',
        'room':'Room',
        'take':'Take a Card',
        'turn':'Turn the cards!',
        'that':'Thats It!',
        'maybe':'May be not...',
        'yourname':'Please, whats your name?',
        'waroom':'Enter the room',
        'picked':'Card Picked!'
    },
    'pt-BR':{
        'createroom':'Criar uma Mesa',
        'enterroom':'Entrar em uma Mesa',
        'help':'Ajuda',
        'room':'Mesa',
        'take':'Pegar uma carta',
        'turn':'Virar as cartas!',
        'that':'Manda Bala!',
        'maybe':'Xi...não é..',
        'yourname':'Por favor, qual seu nome?',
        'waroom':'Digite a sala',
        'picked':'Carta Selecionada!'
    }
};

var LANG = window.navigator.language;

function spoke(index) {
    if ($(sentence[LANG]).length) {
        return sentence[LANG][index];
    } else {
        return sentence['en'][index];
    }
}