if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/sw.js')
      .then(reg => console.info('registered sw', reg))
      .catch(function(error) {
        console.log('Service worker registration failed, error:', error);
      });
}
