var PRECACHE_URLS = [
  '/',
  '/app.js',
  '/sw.js',
  '/manifest.json',
'/css/index.css',
'/favicon.ico',
'/img/browserconfig.xml',
'/img/apple-icon-144x144.png',
'/img/apple-icon-114x114.png',
'/img/android-icon-72x72.png',
'/img/favicon-32x32.png',
'/img/icons-192.png',
'/img/logo.png',
'/img/apple-icon-76x76.png',
'/img/ms-icon-310x310.png',
'/img/apple-icon-152x152.png',
'/img/apple-icon-precomposed.png',
'/img/android-icon-144x144.png',
'/img/ms-icon-150x150.png',
'/img/apple-icon-60x60.png',
'/img/ms-icon-70x70.png',
'/img/cards/style_00/05.jpg',
'/img/cards/style_00/03.jpg',
'/img/cards/style_00/13.jpg',
'/img/cards/style_00/02.jpg',
'/img/cards/style_00/01.jpg',
'/img/cards/style_00/08.jpg',
'/img/manifest.json',
'/img/android-icon-96x96.png',
'/img/apple-icon-57x57.png',
'/img/favicon-96x96.png',
'/img/apple-icon-180x180.png',
'/img/favicon-16x16.png',
'/img/android-icon-192x192.png',
'/img/android-icon-48x48.png',
'/img/android-icon-36x36.png',
'/img/apple-icon-72x72.png',
'/img/icons-512.png',
'/img/apple-icon.png',
'/img/apple-icon-120x120.png',
'/img/ms-icon-144x144.png',
'/index.html',
'/js/jquery-3.3.1.min.js',
'/js/language.js',
'/js/index.js'
];


var version = "00.09.99";
var cacheName = 'underpoker' + version;
var CACHE_DYNAMIC_NAME = 'underpoker' + parseInt(Math.random() * 100000);
var CACHE_CONTAINING_ERROR_MESSAGES  = 'underpokere_rror'

self.addEventListener('install', e => {
  e.waitUntil(
    caches.open(cacheName).then(cache => {
      return cache.addAll(PRECACHE_URLS)
          .then(() => self.skipWaiting());
    })
  );
});

self.addEventListener('activate', event => {
  event.waitUntil(self.clients.claim());
});

addEventListener('fetch', function(event) {
  event.respondWith(
    caches.match(event.request)
      .then(function(response) {
        if (response) {
          return response;     // if valid response is found in cache return it
        } else {
          console.log(event.request);
          console.log(event.request.url);
          if (event.request.url.substr(0,13)=='https://wiwon') {
            return fetch(event.request)     //fetch from internet
            .then(function(res) {
                  return res;   // return the fetched data
                })
          }
          return fetch(event.request)     //fetch from internet
            .then(function(res) {
              return caches.open(CACHE_DYNAMIC_NAME)
                .then(function(cache) {
                  console.log(res);
                  cache.put(event.request.url, res.clone());    //save the response for future
                  return res;   // return the fetched data
                })
            })
            .catch(function(err) {       // fallback mechanism
              return caches.open(CACHE_CONTAINING_ERROR_MESSAGES)
                .then(function(cache) {
                  return cache.match('/offline.html');
                });
            });
        }
      })
  );
});   


