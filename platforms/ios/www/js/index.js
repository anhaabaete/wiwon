
var app = 
{
    // Application Constructor
    initialize: function() 
    {
        document.addEventListener('deviceready', this.onDeviceReady(), false);
    },

    onDeviceReady: function() 
    {
        this.goHome();
    },

    /**
     * goHome
     * Go to the main menu start page
     */
    goHome: function() 
    {
        if (!$("#homescreen").length) {
            this.createObject('div','homescreen','homeScreen','.app');
        }
        this.createObject('div','menu_odd',null,'#homescreen');
        this.createObject('div','menucell_odd',null,'#menu_odd');
        this.createObject('div','menu_pair',null,'#homescreen');
        this.createObject('div','menucell_pair',null,'#menu_pair');
        this.menu();
    },

    menu: function() 
    {
        var size = Object.keys(menu).length;
        for (var i = 0; i < size; i++) 
        {
            if (i % 2) 
            { // odd
                $("#menucell_odd").append('<button id="' + menu[i].action +
                '" class="itenmenu odditem">'+
                menu[i].label +
                '</button>');
                document.getElementById(menu[i].action)
                    .addEventListener("click", this.action);
            }
            else 
            { // pair
                $("#menucell_pair").append('<button id="' + menu[i].action +
                '" class="itenmenu odditem">'+
                menu[i].label +
                '</button>');
                document.getElementById(menu[i].action)
                    .addEventListener("click", this.action);
            }



        }
    },

    action: function(act) {
        switch (this.id) {
            case 'enterroom':
                app.enterroom(this.id,'sala=0');
                break;
            case 'createroom':
                console.warn(this.id);
                break;
            case 'help':
                console.warn(this.id);
                break;
            default:
                alert('Menu miss: 0 Contact the developers');
                break;
        }
    },

    /**
     * createObject
     * 
     * Create element Object on HTML app scene
     * The name maybe used to class too with you writed using '_'
     * 
     * @param {String} type 
     * @param {String} name 
     * @param {String} clsName 
     * @param {String} context 
     */
    createObject: function(type, name, clsName, context) 
    {
        if (!clsName) { clsName = name.split('_')[0]; }
        var obj = document.createElement(type);
        obj.id = name;
        obj.className = clsName;
        $(context).append(obj);
        return obj;
    },

    enterroom: function(serv,data) {
        app.waiting(true);
        $.ajax({
            url:ROOTURL+serv,
            type:'post',
            dataType:'json',
            crossDomain: true,
            data: data,
            complete: function(x,s) {
                app.waiting(false);
                if (s=='success') {
                    app.manifestRoom(x.responseJSON.room);
                } else {
                    console.error(s + ' ' + x.status + ": " + x.statusText);
                }
            }
        });
    },

    manifestRoom: function(room) {
        $(".menu").remove();

        var turncards = app.createObject('button','turncards','turnCards','#homescreen');
        var roomnumber = app.createObject('div','roomnumber','roomNumber','#homescreen');
        $(roomnumber).html("Room: <span>"+room+"</span>");
        $(turncards).html("Turn The Cards!");

        app.putElMiddle(turncards);

        $(turncards).click(function() {
            app.waiting(true);
            $.ajax({
                url:ROOTURL+'turncards',
                type:'post',
                dataType:'json',
                crossDomain: true,
                data: "room="+$("#roomnumber span").innerText,
                complete: function(x,s) {
                    app.waiting(false);
                    if (s=='success') {
                        $("#turncards").remove();
                        var size = Object.keys(x.responseJSON).length;

                        var divcards = app.createObject('div',
                                    'divimgcard','divimgcard','#homescreen');

                        var cards = new Array();
                        var minor = 14;
                        var major = 0;
                        for (var i = 0; i < size; i++) {
                            cards[i] = app.createObject('div',
                                    'imgcard'+i,'imgcard','#divimgcard');
                            $(cards[i]).html(
                                '<img src="img/cards/style_00/' + 
                                    ('0'+x.responseJSON[i].card).substr(-2) +'.jpg">'+
                                   '<label>' + x.responseJSON[i].name + '</label>');
                            if (x.responseJSON[i].card<minor) {
                                minor = i;
                            }
                            if (x.responseJSON[i].card>major) {
                                major = i;
                            }
                        }

                        $(cards[major]).css({
                            "box-shadow":'0 0 1em gold, 0 0 1em red',
                            'background-color':'#222'});
                        $(cards[minor]).css({
                            "box-shadow":'0 0 1em white, 0 0 1em blue',
                            'background-color':'#222'});

                        setTimeout(function() {
                        app.putElCenter(divcards);
                        app.putElMiddle(divcards);

                        $(divcards).css({'visibility':'visible'});
                        }, 100);

                    } else {
                        console.error(s + ' ' + x.status + ": " + x.statusText);
                    }
                }
            });
        });

    },

    waiting: function(k) {
        if (k) {
            $("#wait").css({'visibility':'visible'});
        } else {
            $("#wait").css({'visibility':'hidden'});
        }
    },

    putElMiddle: function(obj) {
        var half = parseInt($(obj).outerHeight()/2);
        $(obj).css({'top':'50%','margin-top':'-'+half+'px'});
    },

    putElCenter: function(obj) {
        var half = parseInt($(obj).outerWidth()/2);
        $(obj).css({'left':'50%','margin-left':'-'+half+'px'});
    }

};

ROOTURL = location.protocol+'//'+location.host+location.pathname;

var menu = 
{
    0:{'label':'Enter A Room','action':'enterroom'},
    1:{'label':'Create A Room','action':'createroom'},
    2:{'label':'Help','action':'help'}
};

app.initialize();